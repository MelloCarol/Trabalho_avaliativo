//
//  ViewController.swift
//  Trabalho_avaliativo
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let email: String
    let endereco: String
    let telefone: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableview.dequeueReusableCell(withIdentifier: "CelulaContato",for: indexPath) as! CellContato
        let contato = listaContatos[indexPath.row]
        
        celula.nome.text = contato.nome
        celula.email.text = contato.email
        celula.endereco.text = contato.endereco
        celula.telefone.text = contato.telefone
        
        return celula
    }
    

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        
        listaContatos.append(Contato(nome: "Noah", email: "noah@gmail.com", endereco: "Alameda dos Jambreiros, 23",telefone: "00000-0000"))
        listaContatos.append(Contato(nome: "Arthur", email: "arthur@gmail.com", endereco: "Rua dos Coqueiros, 190",telefone: "11111-1111"))
        listaContatos.append(Contato(nome: "Lucas", email: "lucas@gmail.com", endereco: "Avenida do Contorno, 1201",telefone: "22222-2222"))
        
    }


}

